# Build the image

```
kameleon build jessie_custom.yaml --enable-cache
```

And follow https://www.grid5000.fr/mediawiki/index.php/Environments_creation_using_Kameleon_and_Puppet
